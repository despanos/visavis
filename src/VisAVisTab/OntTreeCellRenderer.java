package VisAVisTab;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import edu.stanford.smi.protege.util.*;


/* A tree cell renderer for the ontology JTree
 *
 */

class OntTreeCellRenderer extends DefaultTreeCellRenderer {

	
	public OntTreeCellRenderer() {
		classIcon = ComponentUtilities.loadImageIcon(OntTreeCellRenderer.class, "resource/PrimitiveClass.gif");
                dataPropIcon = ComponentUtilities.loadImageIcon(OntTreeCellRenderer.class, "resource/DatatypeSlot.gif");
		objPropIcon = ComponentUtilities.loadImageIcon(OntTreeCellRenderer.class, "resource/ObjectSlot.gif");
                annotPropIcon = ComponentUtilities.loadImageIcon(OntTreeCellRenderer.class, "resource/Annotation.gif");
		propIcon = ComponentUtilities.loadImageIcon(OntTreeCellRenderer.class, "resource/Property.gif");

	}


    public Component getTreeCellRendererComponent(
                        JTree tree,
                        Object value,
                        boolean sel,
                        boolean expanded,
                        boolean leaf,
                        int row,
                        boolean hasFocus) {

        super.getTreeCellRendererComponent(
                        tree, value, sel,
                        expanded, leaf, row,
                        hasFocus);

		DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
		if (node != null) {
                        NodeInfo myInfo = (NodeInfo)node.getUserObject();
                        
                        if (myInfo.type==NodeInfo.CLASS) {
                        
			Font font = getFont();
                        
                        if (myInfo.type==ClassInfo.CLASS){
                           setFont(font.deriveFont(Font.BOLD));
			   setIcon(classIcon); 
                           }
                        else if (myInfo.type==ClassInfo.THING){
                            setFont(font.deriveFont(Font.BOLD));
			    setIcon(classIcon); 
                         }
                        }
                        else if (myInfo.type==NodeInfo.PROPERTY){
                        PropertyInfo propInfo = myInfo.propertyInfo;
                        Font font = getFont();
			switch (propInfo.type) {
				case PropertyInfo.OBJ_PROPERTY:
					setFont(font.deriveFont(Font.PLAIN));
					setIcon(objPropIcon);
					break;
				case PropertyInfo.DATAT_PROPERTY:
					setFont(font.deriveFont(Font.PLAIN));
					setIcon(dataPropIcon);
					break;
                                case PropertyInfo.ANNOT_PROPERTY:
					setFont(font.deriveFont(Font.PLAIN));
					setIcon(annotPropIcon);
					break;      
				case PropertyInfo.OTHER:
					setFont(font.deriveFont(Font.PLAIN));
					setIcon(propIcon);
					break;
			}
		}
                
                }

        return this;
    }

	ImageIcon classIcon;
	ImageIcon dataPropIcon;
	ImageIcon objPropIcon;
        ImageIcon annotPropIcon;

	ImageIcon propIcon;
}

