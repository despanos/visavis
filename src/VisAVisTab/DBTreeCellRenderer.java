

package VisAVisTab;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import edu.stanford.smi.protege.util.*;


/* A tree cell renderer for the Database JTree
 *
 */

class DBTreeCellRenderer extends DefaultTreeCellRenderer {

	
	public DBTreeCellRenderer() {
		sysTableIcon = ComponentUtilities.loadImageIcon(DBTreeCellRenderer.class, "resource/table_system.gif");
                tableIcon = ComponentUtilities.loadImageIcon(DBTreeCellRenderer.class, "resource/table.gif");
		viewIcon = ComponentUtilities.loadImageIcon(DBTreeCellRenderer.class, "resource/view.gif");

		pkIcon = ComponentUtilities.loadImageIcon(DBTreeCellRenderer.class, "resource/pk.gif");
                fkIcon = ComponentUtilities.loadImageIcon(DBTreeCellRenderer.class, "resource/fk.gif");
                pfkIcon = ComponentUtilities.loadImageIcon(DBTreeCellRenderer.class, "resource/pfk.gif");

	}


    public Component getTreeCellRendererComponent(
                        JTree tree,
                        Object value,
                        boolean sel,
                        boolean expanded,
                        boolean leaf,
                        int row,
                        boolean hasFocus) {

        super.getTreeCellRendererComponent(
                        tree, value, sel,
                        expanded, leaf, row,
                        hasFocus);
                
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)value;
		if (node != null) {
			TreeNodeInfo info = (TreeNodeInfo)node.getUserObject();

			Font font = getFont();
			switch (info.m_type) {
				case TreeNodeInfo.TABLE_SYSTEM:
					setFont(font.deriveFont(Font.ITALIC));
					setIcon(sysTableIcon);
					break;
				case TreeNodeInfo.TABLE:
					setFont(font.deriveFont(Font.BOLD));
					setIcon(tableIcon);
					break;
				case TreeNodeInfo.TABLE_VIEW:
					setFont(font.deriveFont(Font.PLAIN));
					setIcon(viewIcon);
					break;
				case TreeNodeInfo.COLUMN:
					setFont(font.deriveFont(Font.PLAIN));
					break;
				case TreeNodeInfo.COLUMN_PK:
					setFont(font.deriveFont(Font.BOLD));
					setIcon(pkIcon);
					break;
                                case TreeNodeInfo.COLUMN_FK:
					setFont(font.deriveFont(Font.BOLD));
					setIcon(fkIcon);
					break;
                                case TreeNodeInfo.COLUMN_PFK:
					setFont(font.deriveFont(Font.BOLD));
					setIcon(pfkIcon);
			}
		}

        return this;
    }

	ImageIcon sysTableIcon;
	ImageIcon tableIcon;
	ImageIcon viewIcon;

        ImageIcon fkIcon;
	ImageIcon pkIcon;
        ImageIcon pfkIcon;
}

