
package VisAVisTab;

import javax.swing.*;
import javax.swing.table.*;

import java.sql.*;

import java.util.*;

import edu.stanford.smi.protege.model.*;


///////////////////////////////////////////////////////////////////////////////
// JDBCDataTable

// The JDBCDataTable class is a Table model that provides data to a JTable

class JDBCDataTable extends AbstractTableModel {
    
    public JDBCDataTable() {
    }
    
    // Execute a query to obtain data for the model
    
    public boolean executeQuery(ConnectInfo connectInfo, String strQuery, String databaseSelected, long numRows) {
        try {
            Class.forName(connectInfo.m_driver);
            con = DriverManager.getConnection(connectInfo.m_DSN, connectInfo.m_UID, connectInfo.m_PWD);
            
            //in case of a mySQL connection,first select which database to use
            if (connectInfo.m_DB.equals("MySQL")){
                PreparedStatement myStmt;
                String query ="USE "+databaseSelected;
                myStmt = con.prepareStatement(query);
                myStmt.execute(query);
            }

                stmt = con.createStatement();
                resultSet = stmt.executeQuery(strQuery);
           
            
            rsMetaData = resultSet.getMetaData();
            
            int numColumns = rsMetaData.getColumnCount();
            columnNames = new String[numColumns];
            for (int i = 0; i < numColumns; i++)
                columnNames[i] = rsMetaData.getColumnLabel(i+1);
            
            rows.removeAllElements();
            int count = 0;
            while (resultSet.next()) {
                Vector newRow = new Vector();
                for (int i = 0; i < numColumns; i++) {
                    int type = rsMetaData.getColumnType(i+1);
                    Object obj = resultSet.getObject(i+1);
                    newRow.addElement(Global.getSQLProtegeObject(type,obj));
                }
                rows.addElement(newRow);
                count++;
                if (count == numRows)
                    break;
            }
            resultSet.close();
            stmt.close();
            con.close();
            
            fireTableChanged(null);
            return true;
        } catch (SQLException ex) {
            Global.defaultSQLExceptionHandler(null,ex);
            return false;
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null,"Class Not Found","Error",JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
    
    // Implementation of TableModel interface
    
    
    // Return the name of a column
    
    public String getColumnName(int columnIndex) {
        if (columnNames[columnIndex] != null)
            return columnNames[columnIndex];
        else
            return "";
    }
    
    // Return the class type of a column
    
    public Class getColumnClass(int columnIndex) {
        int type;
        try {
            type = rsMetaData.getColumnType(columnIndex+1);
        } catch (SQLException e) {
            return super.getColumnClass(columnIndex);
        }
        
        ValueType protegeType = Global.getSQLProtegeType(type);
        return protegeType.getJavaType();
    }
    
    // Return the number of rows in this model
    
    public int getRowCount() {
        return rows.size();
    }
    
    // Return the number of columns contained in this model
    
    public int getColumnCount() {
        return columnNames.length;
    }
    
    // Return the data residing at a row and column
    
    public Object getValueAt(int rowIndex, int columnIndex) {
        Vector row = (Vector)rows.elementAt(rowIndex);
        return row.elementAt(columnIndex);
    }
    
    
    private Connection con;
    private Statement stmt;
    private ResultSet resultSet;
    private ResultSetMetaData rsMetaData;
    
    private String[] columnNames = {};
    private Vector rows = new Vector();
}

