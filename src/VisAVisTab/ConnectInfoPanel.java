

package VisAVisTab;



import com.hp.hpl.jena.ontology.*;
import com.hp.hpl.jena.rdf.model.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;



///////////////////////////////////////////////////////////////////////////////
// ConnectInfoPanel

// ConnectInfoPanel class contains a JComboBox that will let the user choose which type of database will be used

class ConnectInfoPanel extends JPanel implements ActionListener {
    
    
    JButton btnChoose;
    JComboBox cmbDSN;
    DataPreviewPanel previewPanel;
    PostgresPanel pgPanel;
    MySQLPanel mySQLPanel;
    OntologyPanel ontPanel;
    int i=0;
    
    
    public ConnectInfoPanel(OntologyPanel ontPanel) {
        
        this.ontPanel=ontPanel;
        Vector dsnList = new Vector();
        
        setLayout(new BorderLayout());
        
        
        
        JPanel p1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        p1.add(new JLabel("Database Type", JLabel.LEFT));
        dsnList.add("MySQL");
        dsnList.add("PostgreSQL");
        cmbDSN = new JComboBox(dsnList);
        cmbDSN.setEditable(false);
        p1.add(cmbDSN);
        
        btnChoose = new JButton("Choose");
        btnChoose.setMnemonic('C');
        btnChoose.addActionListener(this);
        p1.add(btnChoose);
        
        add(p1,BorderLayout.NORTH);
    }
    
    // Handle action events
    
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == btnChoose) {
            
            //if MySQL is selected in the JComboBox
            if (((String)cmbDSN.getSelectedItem()).compareToIgnoreCase("MySQL")==0){
                /* remove the second component that has been added to the panel
                 so that MySQLPanel is not painted on top of it*/
                if (i!=0) remove(1);
                
                mySQLPanel=new MySQLPanel(ontPanel);
                pgPanel=null;
                add(mySQLPanel,BorderLayout.CENTER);
                
                if (i == 0) i++;
            } 
            
            //if PostgreSQL is selected in the JComboBox
            else if (((String)cmbDSN.getSelectedItem()).compareToIgnoreCase("PostgreSQL")==0){
                /* remove the second component that has been added to the panel
                 so that PostgresPanel is not painted on top of it*/
                if (i!=0) remove(1);
                
                pgPanel=new PostgresPanel(ontPanel);
                mySQLPanel=null;
                add(pgPanel,BorderLayout.CENTER);
                
                if (i==0) i++;
            }
            
            
            
        }
        
    }
}
