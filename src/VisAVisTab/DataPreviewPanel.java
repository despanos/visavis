

package VisAVisTab;


import java.awt.*;
import java.awt.dnd.*;
import java.awt.event.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;



///////////////////////////////////////////////////////////////////////////////
// DataPreviewPanel

// The DataPreviewPanel class encapsulates a table used to display a preview
// of a table to the user.

class DataPreviewPanel extends JPanel implements ItemListener, ActionListener,DropTargetListener {
    
    
    public DataPreviewPanel() {
        setLayout(new BorderLayout());
        setAlignmentX(Component.CENTER_ALIGNMENT);
        setAlignmentY(Component.CENTER_ALIGNMENT);
        
        // add UI components
        JPanel optionsPanel = new JPanel(new GridLayout(1,2));
        
        JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        previewCheckBox = new JCheckBox("Preview");
        previewCheckBox.setSelected(true);
        previewCheckBox.setMnemonic('P');
        previewCheckBox.addItemListener(this);
        previewCheckBox.setEnabled(false);
        p1.add(previewCheckBox);
        
        JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        numRowsField = new JTextField(Long.toString(numRows),6);
        numRowsField.setEnabled(false);
        DropTarget dropTarget=new DropTarget(numRowsField,this);
        
        p2.add(new JLabel("Number of rows"));
        p2.add(numRowsField);
        p2.add(new JLabel("(0 = all)"));
        setNumRowsButton = new JButton("Set");
        setNumRowsButton.setMnemonic('e');
        setNumRowsButton.addActionListener(this);
        setNumRowsButton.setEnabled(false);
        p2.add(setNumRowsButton);
        
        optionsPanel.add(p1);
        optionsPanel.add(p2);
        add(optionsPanel,BorderLayout.NORTH);
        
        dataModel = new JDBCDataTable();
        tableData = new JTable(dataModel);
        JScrollPane sp = new JScrollPane(tableData);
        sp.setPreferredSize(new Dimension(200,300));
        add(sp,BorderLayout.CENTER);
    }
    
    // Handle state changed events of the Preview check box
    
    public void itemStateChanged(ItemEvent e) {
        Object item = e.getItem();
        int newState = e.getStateChange();
        if (item == previewCheckBox) {
            if (newState == ItemEvent.DESELECTED)
                deleteTable();
            else if (newState == ItemEvent.SELECTED);
            updateTable2();
        }
    }
    
    // Handle action events
    
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        if (src == setNumRowsButton) {
            //check whether the text filed contains a number
            Pattern p=Pattern.compile("\\d");
            Matcher m = p.matcher(numRowsField.getText());
            boolean b = m.matches();
            if (b){
                numRows = Long.parseLong(numRowsField.getText());
                updateTable2();
            }
            else{
               JOptionPane.showMessageDialog(null,"Put only numbers in this field",
                            "Not a number",
                            JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    //reject drops 
    public void drop(DropTargetDropEvent event) {
        if (event.getDropTargetContext().getComponent()==numRowsField)
                event.rejectDrop();
            
    }
    
    public void dragExit(DropTargetEvent event) {
        //System.out.println( "dragExit");
    }
    
    public void dropActionChanged( DropTargetDragEvent event ) {
    }
    
    
    public void dragOver(DropTargetDragEvent event) {
        // System.out.println( "dragOver");
    }
    
    public void dragEnter(DropTargetDragEvent event) {
        
    }
    
    // Called by other classes to display a preview of another table
    
    public void updateTable(ConnectInfo connectInfo, String query, String databaseSelected) {
        this.connectInfo = connectInfo;
        queryString = query;
        this.databaseSelected=databaseSelected;
        updateTable2();
    }
    
    // Update the table with data from a new table
    
    private void updateTable2() {
        if (previewCheckBox.isSelected()) {
            boolean bOK = dataModel.executeQuery(connectInfo,queryString,databaseSelected,numRows);
            if (!bOK) {
                deleteTable();
                
                previewCheckBox.setEnabled(false);
                numRowsField.setEnabled(false);
                setNumRowsButton.setEnabled(false);
            } else {
                previewCheckBox.setEnabled(true);
                numRowsField.setEnabled(true);
                setNumRowsButton.setEnabled(true);
            }
        }
    }
    
    // Return the current data model
    
    public JDBCDataTable getTable() {
        return dataModel;
    }
    
    // Clear the contents of the table
    
    public void deleteTable() {
        dataModel = new JDBCDataTable();
        tableData.setModel(dataModel);
    }
   
    
    private JTable tableData;
    private JDBCDataTable dataModel;
    
    private JCheckBox previewCheckBox;
    
    private JTextField numRowsField;
    private JButton setNumRowsButton;
    
    private long numRows = 0;
    
    private ConnectInfo connectInfo = new ConnectInfo();
    private String queryString,databaseSelected;
}

