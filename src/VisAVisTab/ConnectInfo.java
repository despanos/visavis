

package VisAVisTab;


import java.lang.*;


///////////////////////////////////////////////////////////////////////////////
// ConnectInfo

// The ConnectInfo class encapsulates the information needed to connect to 
// a data source.  That includes the driver, data source name, user login, and
// password.

class ConnectInfo extends Object {


	public ConnectInfo() {
		set("","","","","");
	}

	public ConnectInfo(String driver, String DSN, String UID, String PWD,String DB) {
		set(driver,DSN,UID,PWD,DB);
	}

	// Set the class data

	public void set(String driver, String DSN, String UID, String PWD,String DB) {
		m_driver = driver;
		m_DSN = DSN;
		m_UID = UID;
		m_PWD = PWD;
                m_DB=DB;
	}

	
	public String toString() {
		return "DSN=" + m_DSN + ";UID=" + m_UID + ";PWD=" + m_PWD+";DB="+m_DB;
	}

	// Class data
	public String m_driver;
	public String m_DSN;
	public String m_UID;
	public String m_PWD;
        public String m_DB;
}


