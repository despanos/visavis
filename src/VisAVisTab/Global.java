

package VisAVisTab;


import java.sql.*;

import edu.stanford.smi.protege.model.*;

import javax.swing.*;

import java.math.*;



// Global

// The Global class contains common services used by other objects

class Global extends Object {

	// Information about the tab and the current version

	static public final String tabName = "VisAVis";
	static public final String tabVersion ="v1.0b";

	// return a Protege equivalent value type of a SQL data type.

	static public ValueType getSQLProtegeType(int sqlType) {	
        switch(sqlType) {
        case Types.BIT:
			return ValueType.BOOLEAN;
			// return ValueType.CLS;
        case Types.FLOAT:
        case Types.DOUBLE:
		case Types.REAL:
		case Types.NUMERIC:
		case Types.DECIMAL:
			return ValueType.FLOAT;
			// return ValueType.INSTANCE;
        case Types.TINYINT:
        case Types.SMALLINT:
        case Types.INTEGER:
        case Types.BIGINT:
			return ValueType.INTEGER;
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
        case Types.DATE:
        case Types.TIME:
        case Types.TIMESTAMP:
			return ValueType.STRING;
			// return ValueType.SYMBOL;
		case Types.ARRAY:
		case Types.BINARY:
		case Types.BLOB:
		case Types.CLOB:
		// Types.DISTINT:
		case Types.JAVA_OBJECT:
		case Types.LONGVARBINARY:
		case Types.NULL:
		case Types.OTHER:
		case Types.REF:
		case Types.STRUCT:
		case Types.VARBINARY:
        default:		
			return ValueType.ANY;
        }
	}

	// Similar to the above method, but converts a SQL object to
	// a Protege object.


	static public Object getSQLProtegeObject(int sqlType, Object obj) {	
		if (obj == null)
			return obj;

        switch(sqlType) {
        case Types.BIT:
			return (Boolean)obj;

        case Types.FLOAT:
			return new Float(((Double)obj).floatValue());
        case Types.DOUBLE:
            
            // because Protege only has type FLOAT for floating point number
            // and not a type DOUBLE for double (Double.class), we need to convert to Float
			// will get an exception otherwise
			return new Float(((Double)obj).floatValue());
		case Types.REAL:
			return (Float)obj;
		case Types.NUMERIC:
			return new Float(((BigDecimal)obj).floatValue());
		case Types.DECIMAL:
			return new Float(((BigDecimal)obj).floatValue());

        case Types.TINYINT:
			 return new Integer(((Byte)obj).intValue());
			//return (Integer)obj;
        case Types.SMALLINT:
			//return new Integer(((Short)obj).intValue());
			return (Integer)obj;
        case Types.INTEGER:
			return obj;
        case Types.BIGINT:
			//return new Integer(((BigInteger)obj).intValue());
			return new Integer(((Long)obj).intValue());

        case Types.CHAR:
			return obj.toString();
        case Types.VARCHAR:
			return obj.toString();
        case Types.LONGVARCHAR:
			return (String)obj;
        case Types.DATE:
			return obj.toString();
        case Types.TIME:
			return obj.toString();
        case Types.TIMESTAMP:
			return obj.toString();

		case Types.ARRAY:
		case Types.BINARY:
		case Types.BLOB:
		case Types.CLOB:
		// Types.DISTINT:
		case Types.JAVA_OBJECT:
		case Types.LONGVARBINARY:
		case Types.NULL:
		case Types.OTHER:
		case Types.REF:
		case Types.STRUCT:
		case Types.VARBINARY:
        default:		
			return obj;
        }
	}

	// The default handler for SQL exceptions in this project

	static public void defaultSQLExceptionHandler(java.awt.Frame parent, SQLException ex) {
		// A SQLException was generated.  Catch it and display the error 
		// information.  Note that there could be multiple error objects 
		// chained together

		while (ex != null) {
			String exceptionString = "Message:  " + ex.getMessage ();
		
			JOptionPane.showMessageDialog(parent,exceptionString+
                                "\nAlso,the version of the database you are using may be out of date!!","Database Connection Problem",JOptionPane.ERROR_MESSAGE);
			ex = ex.getNextException();
		}
	}

}
