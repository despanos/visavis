


package VisAVisTab;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.border.*;

import java.sql.*;

import java.util.*;

import java.io.*;

import edu.stanford.smi.protege.model.*;
import edu.stanford.smi.protege.widget.*;
import edu.stanford.smi.protege.util.*;



///////////////////////////////////////////////////////////////////////////////
// VisAVisTab

// VisAVisTab is the main, top-level class, directly subclassed from AbstractTabWidget

public class VisAVisTab extends AbstractTabWidget {


	public VisAVisTab() {
    }
    
	// Initialize this tab-widget and the GUI layout

    public void initialize() {
        // set tab properties
        setLabel(Global.tabName + " " + Global.tabVersion);

        // add UI components
        GridBagLayout gridbag=new GridBagLayout();
        setLayout(gridbag);
        GridBagConstraints constraints = new GridBagConstraints();
       
        
        JSplitPane split=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        
        
        Border etched = BorderFactory.createEtchedBorder();
        Border dbTitled = BorderFactory.createTitledBorder(etched, "Database Window");
        Border ontTitled = BorderFactory.createTitledBorder(etched, "Ontology Window");
        
        OntologyPanel ontPanel = new OntologyPanel();
	connectInfoPanel = new ConnectInfoPanel(ontPanel);
        OntQueryPanel ontQueryPanel = new OntQueryPanel(connectInfoPanel);
        
        
	ontPanel.setBorder(ontTitled);
        connectInfoPanel.setBorder(dbTitled);
        
        split.setLeftComponent(ontPanel);
        split.setRightComponent(connectInfoPanel);

        
        ontPanel.buildConstraints(constraints,0,0,1,1,100,95,GridBagConstraints.BOTH);
        gridbag.setConstraints(split,constraints);
        add(split);
        
        
        ontPanel.buildConstraints(constraints,0,1,1,1,100,5,GridBagConstraints.BOTH);
        gridbag.setConstraints(ontQueryPanel,constraints);
        add(ontQueryPanel);
        
    }

	// Save the current list of data source names that have been accessed 
	// to disk when the current project is closed.

	public void close() {
		super.close();		
	}

	// Save the current list of data source names that have been accessed 
	// to disk when the current project is saved.

	public void save() {
		super.save();
	}


	private ConnectInfoPanel connectInfoPanel;
        
}

